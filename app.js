
var express = require('express');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var request = require("request");
var jwt = require('jwt-simple');
var dotenv = require('dotenv');

var proxy = require('http-proxy-middleware');

// var sugarBase = process.env.SUGARBASE;
// var secret = process.env.SECRET;
// var adminsecret = process.env.ADMINSECRET;
// var username = process.env.USERNAME;
// var password = process.env.PASSWORD;
// var userHeader = process.env.USERHEADER;


var sugarBase = 'http://docker-machine:32773/rest/v10';
var secret = 'xxx';
var adminsecret = '12345';
var username = 'admin';
var password = 'admin';
var userHeader = 'Host';

var app = express();
app.use(cookieParser())

app.set('view engine', 'jade');


// get an instance of router
var router = express.Router();



//check cookies
function userVerify (req, res, next) {
    var cookie = req.cookies.horizonUser;
    if (cookie === undefined) {
        console.log('missing cookie');
        adminVerify (req, res, next);
    } else {
        console.log('cookie exists', cookie);
        var decoded = jwt.decode(cookie, secret);
        userSession (req, res, next, decoded);
    } 
}

//check user session
function userSession (req, res, next, decoded){
    var userToken = 'decoded.token';
    var options = {
        method: 'GET',
        url: sugarBase + '/me',
        headers: {'oauth-token': userToken}
    };
    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        res.setHeader('sugar-bootstrap-access-token', decoded.token);
        res.status(200).send('OK Session');
        next();
      } else {
        userRefresh (req, res, next, decoded)
      }
    });
}

function userRefresh (req, res, next, decoded) {
    var refreshToken = 'decoded.refresh';
    var options = {
        method: 'POST',
        url: sugarBase + '/oauth2/token',
        body: '{"grant_type":"refresh_token","refresh_token":"'+refreshToken+ '","client_id":"sugar","client_secret":""}'
    };
    request(options, function (error, response, body) {
      if (!error && response.statusCode == 201) {
        res.setHeader('sugar-bootstrap-access-token', decoded.token);
        res.status(200).send('user session refreshed');
        next();
      } else {
        adminVerify (req, res, next);
      }
    });
}

//check admin
function adminVerify (req, res, next) {
    var user = req.get(userHeader);
    var cookie = req.cookies.horizonAdmin;
    if (cookie === undefined) {
        console.log('missing admin cookie');
        adminLogin (req, res, next, user);
    } else {
        console.log('admin cookie exists', cookie);
        var decoded = jwt.decode(cookie, adminsecret);
        adminSession (req, res, next, user, decoded);
    } 
}

function adminSession (req, res, next, user, decoded) {
    var adminToken = decoded.token;
    var options = {
        method: 'GET',
        url: sugarBase + '/me',
        headers: {'oauth-token': adminToken}
    };
    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        userLogin (req, res, next, user, decoded);
      } else {
        adminRefresh (req, res, next, user, decoded);
      }
    });
}

function adminRefresh (req, res, next, user, decoded) {
    var refreshToken = decoded.refresh;
    var options = {
        method: 'POST',
        url: sugarBase + '/oauth2/token',
        body: '{"grant_type":"refresh_token","refresh_token":"'+refreshToken+ '","client_id":"sugar","client_secret":""}'
    };
    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        var data = JSON.parse(body);
        var horizonAdmin = {
            token: data.access_token,
            refresh: data.refresh_token,
            download: data.download_token
        }
        var token = jwt.encode(horizonAdmin, adminsecret);
        res.cookie('horizonAdmin',token,{ maxAge: 900000, httpOnly: true });
        userLogin (req, res, next, user, decoded);
      } else {
        adminLogin (req, res, next, user, decoded);
      }
    });
}

function adminLogin (req, res, next, user) {
    var options = {
        method: 'POST',
        url: sugarBase + '/oauth2/token',
        body: '{"grant_type":"password","client_id":"sugar","client_secret":"","username":"'+username+'","password":"'+password+'","platform":"api"}'
    };
    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        var data = JSON.parse(body);
        var horizonAdmin = {
            token: data.access_token,
            refresh: data.refresh_token,
            download: data.download_token
        }
       var token = jwt.encode(horizonAdmin, adminsecret);
       res.cookie('horizonAdmin',token,{ maxAge: 900000, httpOnly: true });
       userLogin (req, res, next, user, horizonAdmin);
      } else {
        res.clearCookie('horizonAdmin');
        res.status(403).send('Admin Invalid');
        next();
      }

    });
}

function userLogin (req, res, next, user, decoded) {
    var adminToken = decoded.token;
    var options = {
        method: 'POST',
        url: sugarBase + '/customv1/oauth2/sudo/' + user,
        headers: {'oauth-token': adminToken },
        body: '{"client_id":"sugar","platform":"api"}'
    };
    request(options, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        var data = JSON.parse(body);
        var horizonUser = {
            token: data.access_token,
            refresh: data.refresh_token,
            download: data.download_token
        }
        var token = jwt.encode(horizonUser, secret);
        res.cookie('horizonUser',token,{ maxAge: 900000, httpOnly: true });
        res.status(200).send('User Valid');
        next();
      } else {
        res.clearCookie('horizonUser');
        res.status(401).send('User Invalid');
        next();
      }
    });
}


//route
router.use(function(req, res, next) {
    if (req.get(userHeader) === undefined) {
        console.log('user pid not found');
        res.status(401).send('User ID Missing').end();
    } else {
      next ();
    }
});

router.get('/authenticate', userVerify, function(req,res) {
    console.log('processed');
    res.end();
});



// apply the routes to our application
app.use('/', router);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;